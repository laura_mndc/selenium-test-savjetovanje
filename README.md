# Testiranje aplikacije Savjetovanje

- Projekt je izrađen u sklopu projekta za kolegij Metode i tehnike testiranja programske podrške
- Testovi se pokreću uz pomoć TestNG okvira koristeći Selenium WebDriver
- Testirana aplikacija dostupna je na
  [Savjetovanje](https://laura_mndc.gitlab.io/savjetovanje/)

![Aplikacija](images/Screenshot_Savjetovanje.png "Screenshot aplikacije")

## Korištene tehnologije i alati

---

- IntelliJ IDEA ([potrebna instalacija](https://www.jetbrains.com/idea/download/#section=windows))
- Maven (za pokretanje iz komandne linije [potrebna instalacija](https://maven.apache.org/download.cgi) - binary zip file)
- TestNG
- Selenium
- Java Development Kit ([potrebna instalacija](https://www.oracle.com/java/technologies/downloads/#jdk17-windows))
- Jenkins ([potrebna instalacija](https://www.jenkins.io/download/))

## Postavljanje projekta u IntelliJ IDEA

---

1. File -> Open... -> odabrati korijenski direktorij projekta
2. Provjeriti jesu li uvezene ovisnosti iz _pom.xml_ datoteke, ako nisu:
   - desni klik na naziv projekta -> Maven -> Reload project

## Pokretanje testova iz IntelliJ IDEA IDE

---

1. Izmijeniti testng.xml datoteku u ovisnosti o željenim testovima za pokretanje
   - Sortirano po internetskim preglednicima
     - defaultno odabrani Google Chrome, Mozilla Firefox i Microsoft Edge
2. Desni klik na _testng.xml_ -> Run

## Pokretanje testova iz komandne linije

---

##### (preduvjet: postavljene environmental varijable za Javu i Maven)

1. Izmijeniti _testng.xml_ datoteku u ovisnosti o željenim testovima za pokretanje
2. Unutar korijenskog direktorija projekta pokrenuti naredbu mvn test

## Izvješća

---

- Kreirana uz pomoć maven surefire plugina
- Spremljena u ./target/surefire-reports
  ![Report](images/Screenshot_Report.png "Screenshot reporta")

## WebDriverManager

---

- Dodana je [WebDriverManager](https://bonigarcia.dev/webdrivermanager/) ovisnost koja automatski preuzima browser driver za odgovarajuću verziju instaliranog preglednika

## Jenkins

---

1. Preuzeti Jenkins s [poveznice](https://www.jenkins.io/download/) (.msi)
2. Pokrenuti installer
3. Popuniti credentials s imenom Windows korisnika i pripadajućom lozinkom
4. Dovršiti instalaciju i označiti pokretanje Jenkinsa kao usluge
5. U pregledniku otvoriti localhost na prethodno odabranom portu (najčešće 8080)
6. Instalirati predložene plugine
7. Unijeti podatke o adminu
8. Odabrati Manage Jenkins
9. Odabrati Global Tool Configuration
10. Dodati putanje za JDK i Maven
11. Dodati novi item: Freestyle project
12. Postaviti Source Code Management za git projekt
13. Dodati _Invoke top-level Maven targets_ kao Build step
14. Pod goals upisati _test_
15. U Advanced unijet putanju do _pom.xml_
    ![Build step](images/Screenshot_Build_Step.png "Screenshot Jenkins")
16. Pokrenuti build na dashboardu
