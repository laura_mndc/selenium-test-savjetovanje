package com.autotest.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage {

    WebDriver driver;

    @FindBy(how = How.ID, using = "email")
    WebElement userEmailInput;

    @FindBy(how = How.ID, using = "password")
    WebElement userPasswordInput;

    @FindBy(how = How.XPATH, using = "//*[@id=\"root\"]/div/div/div/form/button")
    WebElement logInButton;


    public LoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(logInButton));
    }

    public boolean isEmailInputVisible() {
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(userEmailInput));
        return userEmailInput.isDisplayed();
    }

    public boolean isLogInButtonVisible() {
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(logInButton));
        return logInButton.isDisplayed();
    }

    public boolean isPasswordInputVisible() {
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(userPasswordInput));
        return userPasswordInput.isDisplayed();
    }

    public void inputEmail(String email) {
        isEmailInputVisible();
        userEmailInput.sendKeys(email);
    }

    public void inputPassword(String password) {
        isPasswordInputVisible();
        userPasswordInput.sendKeys(password);
    }

    public void logInClick() {
        logInButton.click();
    }

    public void logInSampleUser() {
        userEmailInput.sendKeys("dorina@gmail.com");
        userPasswordInput.sendKeys("dorina111");
        logInClick();
    }


}
