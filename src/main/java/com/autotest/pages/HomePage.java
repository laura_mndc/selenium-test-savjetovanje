package com.autotest.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage {

    WebDriver driver;

    @FindBy(how = How.CSS, using = "div.col-md-4:nth-child(1) > a:nth-child(1) > div:nth-child(1)")
    WebElement advisorFormCard;

    @FindBy(how = How.CSS, using = "div.col-md-4:nth-child(2) > a:nth-child(1) > div:nth-child(1)")
    WebElement FAQCard;

    @FindBy(how = How.CSS, using = "div.col-md-4:nth-child(3) > a:nth-child(1) > div:nth-child(1)")
    WebElement forumCard;

    @FindBy(how = How.CSS, using = "a[href*='/savjetovanje/profile']")
    WebElement profilePageLink;

    @FindBy(how = How.XPATH, using = "//*[@id=\"root\"]/div[1]/nav/div[2]/button")
    WebElement logOutButton;


    public HomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        isFAQCardVisible();
    }

    public void logoutClick() {
        logOutButton.click();
    }

    public void profilePageClick() {
        profilePageLink.click();
    }

    public boolean islogOutButtonVisible() {
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(logOutButton));
        return logOutButton.isDisplayed();
    }

    public boolean isFAQCardVisible() {
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(FAQCard));
        return FAQCard.isDisplayed();
    }

    public void openFAQ() {
        FAQCard.click();
    }

    public void openForum() {
        forumCard.click();
    }

    public void openAdvisorForm() {
        advisorFormCard.click();
    }

    public WebElement getFAQCard() {
        return FAQCard;
    }
}
