package com.autotest.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AdvisorFormPage {

    WebDriver driver;

    @FindBy(how = How.NAME, using = "residence")
    WebElement residenceInput;

    @FindBy(how = How.XPATH, using = "//*[@id=\"root\"]/div[2]/form/div[7]/button")
    WebElement submitButton;

    public AdvisorFormPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(residenceInput));
    }

    public void changeResidence(String newResidence) {
        //wait until value is fetched from the database
        new WebDriverWait(driver, 30).until((ExpectedCondition<Boolean>) driver -> driver.findElement(By.name("residence")).getAttribute("value").length() != 0);
        residenceInput.clear();
        residenceInput.sendKeys(newResidence);
    }

    public void submit() {
        submitButton.click();
    }
}
