package com.autotest.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ProfilePage {

    WebDriver driver;

    @FindBy(how = How.XPATH, using = "//*[@id=\"root\"]/div[2]/div[1]/div/div[2]/p[6]")
    WebElement residenceParagraph;

    @FindBy(how = How.CSS, using = "a[href*='/savjetovanje/']")
    WebElement homePageLink;


    public ProfilePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(residenceParagraph));
    }

    public WebElement getResidenceParagraph() {
        return residenceParagraph;
    }

    public void clickHomePageLink() {
        homePageLink.click();
    }

}
