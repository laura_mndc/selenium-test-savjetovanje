package com.autotest.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ForumPage {

    WebDriver driver;

    @FindBy(how = How.CSS, using = "#root > div.container.mx-auto > div > div.subforum-title")
    WebElement forumTitleLabel;

    @FindBy(how = How.CSS, using = "#root > div.container.mx-auto > div > div.comment > button")
    WebElement newSubjectButton;

    @FindBy(how = How.XPATH, using = "//*[@id=\"newtopic-area\"]/form/button")
    WebElement addSubjectButton;

    @FindBy(how = How.CSS, using = "input.form-control")
    WebElement titleInput;

    @FindBy(how = How.CSS, using = "textarea.form-control")
    WebElement descriptionInput;

    public ForumPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(forumTitleLabel));
    }

    public int getForumSubjectsCount() {
        return driver.findElements(By.xpath("//*[@class='subforum-row']")).size();
    }

    public String getNthTitle(int n) {
        try {
            WebElement title = driver.findElement(By.cssSelector(String.format("div.subforum-row:nth-child(%s) > div:nth-child(1) > h4:nth-child(1) > a:nth-child(1)", n + 1)));
            return title.getText();
        } catch (Exception ex) {
            throw ex;


        }
    }

    public void addSubject(String title, String description) {
        newSubjectButton.click();
        titleInput.sendKeys(title);
        descriptionInput.sendKeys(description);
        descriptionInput.submit();

    }

    public WebElement getAddSubjectButton() {
        return addSubjectButton;
    }


}
