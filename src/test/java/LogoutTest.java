import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.autotest.pages.HomePage;
import com.autotest.pages.LoginPage;

public class LogoutTest {
    //-------------------Global Variables-----------------------------------

    public WebDriver driver;
    public Actions action;
    public WebDriverWait wait;
    //Declare a test URL variable
    public String testURL = "https://laura_mndc.gitlab.io/savjetovanje/";

    LoginPage loginPage;
    HomePage homePage;

    //----------------------Test Setup-----------------------------------
    @BeforeMethod
    @Parameters("browser")
    public void setupTest(String browser) {
        if (browser.equalsIgnoreCase("firefox")) {
            WebDriverManager.firefoxdriver().setup();
            driver = new FirefoxDriver();
        } else if (browser.equalsIgnoreCase("chrome")) {
            WebDriverManager.chromedriver().setup();
            driver = new ChromeDriver();
        } else {
            WebDriverManager.edgedriver().setup();
            driver = new EdgeDriver();
        }


        action = new Actions(driver);
        driver.navigate().to(testURL);
        loginPage = new LoginPage(driver);
        loginPage.logInSampleUser();
        homePage = new HomePage(driver);

    }


    @Test
    public void logoutTest() {
        driver.manage().window().maximize();
        homePage.logoutClick();
        boolean loggedOut = loginPage.isLogInButtonVisible();
        Assert.assertTrue(loggedOut);
    }

    //---------------Test TearDown-----------------------------------
    @AfterMethod
    public void teardownTest() {
        //Close browser and end the session
        driver.quit();
    }
}