import com.autotest.pages.*;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.Random;

public class AddForumSubjectTest {
    //-------------------Global Variables-----------------------------------

    public WebDriver driver;
    public Actions action;
    public WebDriverWait wait;

    //Declare a test URL variable
    public String testURL = "https://laura_mndc.gitlab.io/savjetovanje/";
    Random random;
    LoginPage loginPage;
    HomePage homePage;
    ForumPage forumPage;

    //----------------------Test Setup-----------------------------------
    @BeforeTest
    @Parameters("browser")
    public void setupTest(String browser) {
        if (browser.equalsIgnoreCase("firefox")) {
            WebDriverManager.firefoxdriver().setup();
            driver = new FirefoxDriver();
        } else if (browser.equalsIgnoreCase("chrome")) {
            WebDriverManager.chromedriver().setup();
            driver = new ChromeDriver();
        } else {
            WebDriverManager.edgedriver().setup();
            driver = new EdgeDriver();
        }

        wait = new WebDriverWait(driver, 30);
        action = new Actions(driver);
        random= new Random(System.currentTimeMillis());

        driver.navigate().to(testURL);
        loginPage = new LoginPage(driver);
        loginPage.logInSampleUser();

        homePage = new HomePage(driver);
        homePage.openForum();
        driver.manage().window().maximize();
    }

    @Test(priority = 2)
    public void addExistingSubjectTest() {
        forumPage = new ForumPage(driver);
        int subjectCountBefore = forumPage.getForumSubjectsCount();
        try {
            String firstTitle = forumPage.getNthTitle(1);
            forumPage.addSubject(firstTitle, "Opis.");
            driver.switchTo().alert().accept();
            int subjectCountAfter = forumPage.getForumSubjectsCount();
            Assert.assertEquals(subjectCountBefore, subjectCountAfter);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    @Test(priority = 1)
    public void addNewSubjectTest() {
        forumPage = new ForumPage(driver);
        int subjectCountBefore = forumPage.getForumSubjectsCount();

        forumPage.addSubject("Programiranje "+random.nextInt(50), "Pitanja vezana uz programerske kolegije.");
        WebElement addSubjectButton = forumPage.getAddSubjectButton();
        wait.until(ExpectedConditions.invisibilityOf(addSubjectButton));

        int subjectCountAfter = forumPage.getForumSubjectsCount();
        Assert.assertEquals(subjectCountBefore + 1, subjectCountAfter);
    }

    //---------------Test TearDown-----------------------------------
    @AfterTest
    public void teardownTest() {
        //Close browser and end the session
        driver.quit();
    }
}
