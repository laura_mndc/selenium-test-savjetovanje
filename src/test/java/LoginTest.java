import com.autotest.pages.HomePage;
import com.autotest.pages.LoginPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.*;
import io.github.bonigarcia.wdm.WebDriverManager;

public class LoginTest {
    //-------------------Global Variables-----------------------------------
    public WebDriver driver;
    //Declare a test URL variable
    public String testURL = "https://laura_mndc.gitlab.io/savjetovanje/";

    LoginPage loginPage;
    HomePage homePage;

    //----------------------Test Setup-----------------------------------

    @BeforeMethod
    @Parameters("browser")
    public void setupTest(String browser) {
        if (browser.equalsIgnoreCase("firefox")) {
            WebDriverManager.firefoxdriver().setup();
            driver = new FirefoxDriver();
        } else if (browser.equalsIgnoreCase("chrome")) {
            WebDriverManager.chromedriver().setup();
            driver = new ChromeDriver();
        } else {
            WebDriverManager.edgedriver().setup();
            driver = new EdgeDriver();
        }
        driver.navigate().to(testURL);
        loginPage = new LoginPage(driver);
    }

    @Test
    public void loginTest() {
        loginPage.inputEmail("dorina@gmail.com");
        loginPage.inputPassword("dorina111");
        loginPage.logInClick();
        homePage = new HomePage(driver);
        boolean loggedIn = homePage.islogOutButtonVisible();
        Assert.assertTrue(loggedIn);

    }

    //---------------Test TearDown-----------------------------------
    @AfterMethod
    public void teardownTest() {
        //Close browser and end the session
        driver.quit();
    }
}
