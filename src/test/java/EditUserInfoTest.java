import com.autotest.pages.AdvisorFormPage;
import com.autotest.pages.HomePage;
import com.autotest.pages.LoginPage;
import com.autotest.pages.ProfilePage;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.*;

public class EditUserInfoTest {


    public WebDriver driver;
    public String testURL = "https://laura_mndc.gitlab.io/savjetovanje/";

    LoginPage loginPage;
    HomePage homePage;
    AdvisorFormPage advisorFormPage;
    ProfilePage profilePage;

    @BeforeTest
    @Parameters("browser")
    public void setupTest(String browser) {
        if (browser.equalsIgnoreCase("firefox")) {
            WebDriverManager.firefoxdriver().setup();
            driver = new FirefoxDriver();
        } else if (browser.equalsIgnoreCase("chrome")) {
            WebDriverManager.chromedriver().setup();
            driver = new ChromeDriver();
        } else {
            WebDriverManager.edgedriver().setup();
            driver = new EdgeDriver();
        }

        driver.navigate().to(testURL);
        loginPage = new LoginPage(driver);
        loginPage.logInSampleUser();
        homePage = new HomePage(driver);
        driver.manage().window().maximize();
    }

    @BeforeMethod
    public void openInputForm() {
        //open user input form page
        homePage.openAdvisorForm();
    }

    @Test(dataProvider = "residenceProvider")
    public void editResidenceTest(String newResidence) {
        advisorFormPage = new AdvisorFormPage(driver);
        advisorFormPage.changeResidence(newResidence);
        advisorFormPage.submit();

        homePage.isFAQCardVisible();
        homePage.profilePageClick();
        profilePage = new ProfilePage(driver);
        WebElement residenceParagraph = profilePage.getResidenceParagraph();
        Assert.assertEquals(residenceParagraph.getAttribute("textContent"), newResidence);
    }

    @DataProvider
    public Object[][] residenceProvider() {
        return new Object[][]{{"Novska"}, {"Vukovar"}, {"Osijek"}};
    }

    @AfterMethod
    public void returnToHomePage() {
        profilePage.clickHomePageLink();
    }

    @AfterTest
    public void teardownTest() {
        //Close browser and end the session
        driver.quit();
    }
}

