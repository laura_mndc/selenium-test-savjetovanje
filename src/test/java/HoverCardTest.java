import com.autotest.pages.HomePage;
import com.autotest.pages.LoginPage;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class HoverCardTest {
    //-------------------Global Variables-----------------------------------
//Declare a Webdriver variable
    public WebDriver driver;
    public Actions action;
    //Declare a test URL variable
    public String testURL = "https://laura_mndc.gitlab.io/savjetovanje/";
    LoginPage loginPage;
    HomePage homePage;

    //----------------------Test Setup-----------------------------------
    @BeforeMethod
    @Parameters("browser")
    public void setupTest(String browser) {
        if (browser.equalsIgnoreCase("firefox")) {
            WebDriverManager.firefoxdriver().setup();
            driver = new FirefoxDriver();
        } else if (browser.equalsIgnoreCase("chrome")) {
            WebDriverManager.chromedriver().setup();
            driver = new ChromeDriver();
        } else {
            WebDriverManager.edgedriver().setup();
            driver = new EdgeDriver();
        }

        action = new Actions(driver);
        driver.navigate().to(testURL);
        loginPage = new LoginPage(driver);
        loginPage.logInSampleUser();
        homePage = new HomePage(driver);
    }


    @Test
    public void hoverTest() {
        driver.manage().window().maximize();
        //select 2nd card
        WebElement card = homePage.getFAQCard();
        String cssProperty = "transform";
        String transformStyleBefore = card.getCssValue(cssProperty);
        action.moveToElement(card).perform();
        String transformStyleAfter = card.getCssValue(cssProperty);
        //there should not be a transform property before hovering over the card
        Assert.assertEquals(transformStyleBefore, "none");
        Assert.assertNotEquals(transformStyleAfter, "none");
    }

    //---------------Test TearDown-----------------------------------
    @AfterMethod
    public void teardownTest() {
        //Close browser and end the session
        driver.quit();
    }
}
